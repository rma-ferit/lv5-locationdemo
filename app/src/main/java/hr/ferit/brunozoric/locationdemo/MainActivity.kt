package hr.ferit.brunozoric.locationdemo

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val locationRequestCode = 10
    private lateinit var  locationManager: LocationManager

    private val locationListener = object: LocationListener{
        override fun onProviderEnabled(provider: String?) { }

        override fun onProviderDisabled(provider: String?) { }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }

        override fun onLocationChanged(location: Location?) {
            updateLocationDisplay(location)
        }
    }

    private fun updateLocationDisplay(location: Location?) {
        val lat = location?.latitude ?: 0
        val lon = location?.longitude ?: 0
        locationDisplay.text = "Lat: $lat\nLon: $lon"

        if(location != null && Geocoder.isPresent()) {
            val geocoder = Geocoder(this, Locale.getDefault());
            try {
                val nearByAddresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1
                );

                if (nearByAddresses.size > 0) {
                    val stringBuilder = StringBuilder();
                    val nearestAddress = nearByAddresses[0];
                    stringBuilder.append("\n")
                        .append(nearestAddress.getAddressLine(0))
                        .append("\n")
                        .append(nearestAddress.getLocality())
                        .append("\n")
                        .append(nearestAddress.getCountryName());
                    locationDisplay.append(stringBuilder.toString());
                }
            } catch (e: IOException) {
                e.printStackTrace(); }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        trackLocationAction.setOnClickListener{ trackLocation() }
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private fun trackLocation() {
        if(hasPermissionCompat(locationPermission)){
            startTrackingLocation()
        } else {
            requestPermisionCompat(arrayOf(locationPermission), locationRequestCode)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {

        when(requestCode){
            locationRequestCode -> {
                if(grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    trackLocation()
                else
                    Toast.makeText(this, R.string.permissionNotGranted, Toast.LENGTH_SHORT).show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun startTrackingLocation() {
        Log.d("TAG", "Tracking location")

        val criteria: Criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE

        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try{
            locationManager.requestLocationUpdates(provider, minTime, minDistance, locationListener)
        } catch (e: SecurityException){
            Toast.makeText(this, R.string.permissionNotGranted, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager.removeUpdates(locationListener)
    }
}
